const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const { ApolloServer } = require('apollo-server-express');
const resolvers = require('./resolvers');
const typeDefs = require('./typeDefs')



async function startApolloServer(typeDefs, resolvers) {
	// Uncaught Exceptions
	process.on('uncaughtException', (err) => {
		console.log('Uncaught Exception');
		console.log(err);
		process.exit(1);
	});

	dotenv.config();
	const server = new ApolloServer({typeDefs, resolvers})
	const app = express();
	app.use(express.json());

	// CORS
	if (process.env.NODE_ENV === 'production') {
		const whitelist = ['https://graphql-front.onrender.com']
		corsOptions = {
			origin: function (origin, callback) {
				if (whitelist.indexOf(origin) !== -1) {
					callback(null, true)
				} else {
					callback(new Error('Not allowed by CORS'))
				}
			},
			credentials: true,
			optionsSuccessStatus: 200,
			exposedHeaders: ['set-cookie']
		}
	} else {
		corsOptions = {
			origin: true,
			credentials: true,
			optionsSuccessStatus: 200,
			exposedHeaders: ['set-cookie']
		}
	}
	app.use(cors(corsOptions));

	await server.start();
	
	server.applyMiddleware({app, path: '/graphql'});

	// Handling Unhandled Routes
	app.all('*', (req, res, next) => {
		res.status(404).json({
			status: 404,
			message: 'Not Found',
		});
	});

	const PORT = process.env.PORT || 3000;
	
	app.listen(PORT, () => {
		console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`);
		//console.log(`Server ready at http://localhost:${PORT}${server.graphqlPath} in ${process.env.NODE_ENV} mode on port ${PORT}`);
	})

	// Unhandled Rejections
	process.on('unhandledRejection', (err) => {
		console.log('Unhandled Rejection');
		console.log(err);
		server.close(() => process.exit(1));
	});


	process.on('SIGTERM', () => {
		console.log('SIGTERM RECEIVED. Shutting down application');
		server.close(() => {
			console.log('Process terminated!');
		});
	});
}

startApolloServer(typeDefs, resolvers);
