const taskList = [
  {
    id: 0,
    name: 'Task name 0',
    description: 'Task description 0'
  },
  {
    id: 1,
    name: 'Task name 1',
    description: 'Task description 1'
  },
  {
    id: 2,
    name: 'Task name 2',
    description: 'Task description 2'
  }
];

module.exports = taskList;