const taskList = require('./taskList');

// The resolvers
const resolvers = {
  Query: {
    getTaskList: () => taskList,
    getTask: (obj, { id }) => {
      const task = taskList.find(task => task.id === id)
      return task;
    }
  },
  Mutation: {
    createTask: (obj, args) => {
      const id = taskList.length + 1;
      const { name, description } = args;
      const newTask = {
        id,
        name,
        description
      }
      taskList.push(newTask);
      return newTask;
    },
    deleteTask: (obj, { id }) => {
      const task = taskList.find(task => task.id === id);
      if (task) {
        const taskIndex = taskList.indexOf(task);
        taskList.splice(taskIndex, 1);
        return { id, message: `Task with ID ${id} deleted successfully` }
      } else {
        throw new Error('Task ID not found');
      }
    },
    updateTask: (obj, { id, name, description }) => {
      const task = taskList.find(task => task.id === id);
      if (task) {
        const taskIndex = taskList.indexOf(task);
        if (name) task.name = name;
        if (description) task.description = description;
        taskList[taskIndex] = { id, name, description }; // Update task using index
        return { id, name, description };
      } else {
        throw new Error('Task ID not found');
      }
    },
  }
};

module.exports = resolvers;