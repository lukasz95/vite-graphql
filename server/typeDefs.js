const { gql } = require('apollo-server-express');

// The GraphQL schema in string form
const typeDefs = gql`
  type Task {
    id: Int!
    name: String!
    description: String!
  },
  type Query {
    getTaskList: [Task],
    getTask(id: Int!): Task
  },
  type DeleteMessage {
    id: Int!,
    message: String
  },
  type Mutation {
    createTask(name: String!, description: String!) : Task,
    deleteTask(id:Int!): DeleteMessage,
    updateTask(id:Int!, name: String, description: String): Task
  }
`;

module.exports = typeDefs;