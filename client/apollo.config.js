module.exports = {
  client: {
    service: {
      name: 'vite-graphql',
      // URL to the GraphQL API
      url: 'http://localhost:3600/graphql',
    },
    // Files processed by the extension
    includes: [
      'src/**/*.vue',
      'src/**/*.js',
    ],
  },
}