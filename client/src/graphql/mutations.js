import gql from 'graphql-tag'

export const DELETE_TASK = gql`
	mutation deleteTask($taskId: Int!){
		deleteTask(id: $taskId) {
			id,
			message
  	}
	}
`

export const UPDATE_TASK = gql`
	mutation updateTask($taskId: Int!, $taskName: String!, $taskDescription: String!){
		updateTask(id: $taskId, name: $taskName, description: $taskDescription) {
			id,
			name,
			description
  	}
	}
`

export const CREATE_TASK = gql`
	mutation createTask($taskName: String!, $taskDescription: String!) {
		createTask(name: $taskName, description: $taskDescription) {
			id,
			name,
			description
		}
	}
`