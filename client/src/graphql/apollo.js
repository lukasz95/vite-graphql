import { ApolloClient, InMemoryCache } from '@apollo/client/core'

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  cache,
  uri: import.meta.env.VITE_API_URL
})

export default apolloClient;