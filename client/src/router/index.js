import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/pages/HomePage/HomePage.vue'

const routes = [
  {
    path: '/',
		name: 'HomePage',
    component: HomePage,
  },
  {
    path: '/about',
    name: 'AboutPage',
    component: () => import(/* webpackChunkName: "aboutPage" */ '@/pages/AboutPage/AboutPage.vue')
  },
  {
    path: '/404',
    name: '_404Page',
    component: () => import(/* webpackChunkName: "_404Page" */ '@/pages/_404Page/_404Page.vue')
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/404'
  }
];

const router = createRouter({
	scrollBehavior: () => ({ left: 0, top: 0 }),
	history: createWebHistory(),
	routes
})

export default router