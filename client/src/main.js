import { createApp, provide, h } from 'vue'
import { createPinia } from 'pinia'
import router from './router'
import App from './App.vue'
import { DefaultApolloClient } from '@vue/apollo-composable'
import apolloClient from "./graphql/apollo"

import './styles/main.scss'

const store = createPinia();
const app = createApp({
  setup () {
    provide(DefaultApolloClient, apolloClient)
  },
  render: () => h(App),
})

app.use(router).use(store).mount('#app');
