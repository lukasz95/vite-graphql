import { defineStore } from "pinia";
import apolloClient from "@/graphql/apollo"
import { GET_TASK_LIST } from '@/graphql/queries'
import { DELETE_TASK, UPDATE_TASK, CREATE_TASK } from '@/graphql/mutations'

export const useTaskStore = defineStore('task', {
	state: () => {
		return { 
			taskList: [],
			feedback: false
		}
	},
	getters: {
		getTaskList: (state) => state.taskList,
		getFeedback: (state) => state.feedback
	},
	actions: {
		fetchTaskList() {
			apolloClient
        .query({
          query: GET_TASK_LIST,
        })
        .then(({ data }) => {
					this.taskList = data.getTaskList
        })
				.catch((error) => {
					console.log(error.message)
				});
		},
		createTask(task) {
			apolloClient
        .mutate({
					mutation: CREATE_TASK,
					variables: { taskName: task.taskName, taskDescription: task.taskDescription }
        })
        .then(({ data }) => {
					console.log(data.createTask)
					this.taskList = [
						{
							id: data.createTask.id,
							name: task.taskName,
							description: task.taskDescription
						},
						...this.taskList
					];
					this.feedback = true
        })
				.catch((error) => {
					console.log(error.message)
				});
		},
		deleteTask(taskId) {
			apolloClient
        .mutate({
					mutation: DELETE_TASK,
					variables: { taskId: taskId }
        })
        .then(({ data }) => {
					console.log(data.deleteTask.message)
					this.taskList = this.taskList.filter(task => task.id !== taskId);
        })
				.catch((error) => {
					console.log(error.message)
				});
		},
		updateTask(task) {
			apolloClient
        .mutate({
					mutation: UPDATE_TASK,
					variables: { taskId: task.taskId, taskName: task.taskName, taskDescription: task.taskDescription }
        })
        .then(({ data }) => {
					console.log(data.updateTask)
					this.taskList = this.taskList.map(item => {
						if(item.id === task.taskId) {
							return {
								...item,
								name: task.taskName,
								description: task.taskDescription 
							}
						} else {
							return item;
						}
					})
        })
				.catch((error) => {
					console.log(error.message)
				});
		}
	}
}); 